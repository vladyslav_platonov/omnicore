<?php

return [
    'external' => [
        'dto' => [
            'mapping' => [
                'id' => [
                    'key' => 'id',
                    'default' => 0,
                ],
                'firstName' => [
                    'key' => 'firstName',
                    'default' => '',
                ],
                'lastName' => [
                    'key' => 'lastName',
                    'default' => '',
                ],
                'emailAddress' => [
                    'key' => 'email',
                    'default' => '',
                ],
                'phoneNumber' => [
                    'key' => 'phoneNumber',
                    'default' => '',
                ],
            ],
        ],
    ],
];
