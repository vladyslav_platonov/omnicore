<?php

use app\lib\mapping\DataMapperInterface;
use app\lib\mapping\DefaultDataMapper;
use app\lib\reflection\PropertyAccessorInterface;
use app\lib\reflection\ReflectionPropertyAccessor;
use app\services\user\factories\UserDtoFactory;
use app\services\user\factories\UserDtoFactoryInterface;
use app\services\user\UserDtoProcessor;
use app\services\user\UserDtoProcessorInterface;
use HttpSoft\Message\RequestFactory;
use HttpSoft\Message\ServerRequestFactory;
use HttpSoft\Message\StreamFactory;
use HttpSoft\Message\UploadedFileFactory;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use app\services\user\mapping\UserDtoDataMapper;
use yii\di\Instance;

return [
    ServerRequestFactoryInterface::class => ServerRequestFactory::class,
    RequestFactoryInterface::class => RequestFactory::class,
    StreamFactoryInterface::class => StreamFactory::class,
    UploadedFileFactoryInterface::class => UploadedFileFactory::class,
    UserDtoFactoryInterface::class => [
        'class' => UserDtoFactory::class,
        '__construct()' => [Instance::of(UserDtoDataMapper::class)],
    ],
    DataMapperInterface::class => DefaultDataMapper::class,
    PropertyAccessorInterface::class => ReflectionPropertyAccessor::class,
    UserDtoProcessorInterface::class => UserDtoProcessor::class,
];