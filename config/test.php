<?php

use Webscooby\Yii\Web\WebRequestFactory;
use yii\i18n\PhpMessageSource;

$params = require __DIR__ . '/params.php';
$services = require __DIR__ . '/services.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        WebRequestFactory::class,
    ],
    'defaultRoute' => 'user/validate',
    'language' => 'en-US',
    'container' => [
        'singletons' => $services,
    ],
    'components' => [
        'db' => $db,
        'mailer' => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'udokmeci.phone.validator' => 'udokmeci.phone.validator.php',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
