<?php

declare(strict_types=1);

namespace app\controllers;

use Psr\Http\Message\RequestInterface;
use Yii;
use yii\helpers\Json;
use yii\rest\Controller;

/**
 * Abstraction for all API controllers.
 */
abstract class AbstractApiController extends Controller
{
    private const HTTP_CODE_SUCCESS = 200;
    private const HTTP_CODE_VALIDATION_ERROR = 422;

    /**
     * Extracts json data from request.
     *
     * @param RequestInterface $request PSR7 request
     * @return array Extracted json data
     */
    protected function getRequestData(RequestInterface $request): array
    {
        return Json::decode($request->getBody()->getContents(), true) ?? [];
    }

    /**
     * Prepares data for success response, can be useful to easy change success response format in future.
     *
     * @param array $data Data to be prepared
     * @return array Prepared data
     */
    protected function successResponse(array $data = []): array
    {
        Yii::$app->response->setStatusCode(self::HTTP_CODE_SUCCESS);

        return $data;
    }

    /**
     * Prepares errors for validation failure response, can be useful to easy change failure response format in future.
     *
     * @param array $data Data to be prepared
     * @return array Prepared data
     */
    protected function validationErrorResponse(array $data = []): array
    {
        Yii::$app->response->setStatusCode(self::HTTP_CODE_VALIDATION_ERROR);

        return $data;
    }
}
