<?php

namespace app\controllers;

use app\lib\exceptions\ValidationFailedException;
use app\services\user\UserDtoProcessorInterface;
use Psr\Http\Message\RequestInterface;
use yii\base\Module;

final class UserController extends AbstractApiController
{
    private UserDtoProcessorInterface $userDtoProcessor;

    public function __construct(
        string $id,
        Module $module,
        UserDtoProcessorInterface $userDtoProcessor,
        array $config = []
    ) {
        $this->userDtoProcessor = $userDtoProcessor;

        parent::__construct($id, $module, $config);
    }

    public function actionValidate(RequestInterface $request): array
    {
        try {
            $this->userDtoProcessor->execute($this->getRequestData($request));
        } catch (ValidationFailedException $exception) {
            return $this->validationErrorResponse($exception->getErrors());
        }

        return $this->successResponse();
    }
}
