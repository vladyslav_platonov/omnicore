<?php

declare(strict_types=1);

namespace app\lib\exceptions;

use LogicException;

/**
 * Should be thrown when validation process fails.
 */
final class ValidationFailedException extends LogicException
{
    private const ERROR_MESSAGE = 'Validation failed';

    private array $errors;

    public function __construct(array $errors)
    {
        parent::__construct(self::ERROR_MESSAGE, 1000);

        $this->errors = $errors;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
