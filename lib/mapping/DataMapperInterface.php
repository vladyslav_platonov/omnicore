<?php

declare(strict_types=1);

namespace app\lib\mapping;

/**
 * Contract for all data mappers.
 */
interface DataMapperInterface
{
    /**
     * @param object $object Some object to set data
     * @param array<string, mixed> $data Raw data (key => value)
     */
    public function map(object $object, array $data): void;
}
