<?php

declare(strict_types=1);

namespace app\lib\mapping;

use app\lib\reflection\PropertyAccessorInterface;

/**
 * Default class to set raw data into some specific object. Uses reflection property accessor to set data.
 */
final class DefaultDataMapper implements DataMapperInterface
{
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    /**
     * @inheritDoc
     */
    public function map(object $object, array $data): void
    {
        foreach ($this->propertyAccessor->getProperties($object) as $property) {
            $property->setAccessible(true);

            if (isset($data[$property->name])) {
                $property->setValue($object, $data[$property->name]);
            }
        }
    }
}
