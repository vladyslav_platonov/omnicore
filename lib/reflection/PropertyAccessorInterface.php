<?php

declare(strict_types=1);

namespace app\lib\reflection;

use ReflectionProperty;

/**
 * Helps to access some object properties.
 */
interface PropertyAccessorInterface
{
    /**
     * @return ReflectionProperty[] Extracted properties
     */
    public function getProperties(object $object): iterable;
}
