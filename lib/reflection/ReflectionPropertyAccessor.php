<?php

declare(strict_types=1);

namespace app\lib\reflection;

use ReflectionClass;
use ReflectionProperty;

/**
 * Extracts properties from some specific object with reflection.
 */
final class ReflectionPropertyAccessor implements PropertyAccessorInterface
{
    /**
     * @inheritDoc
     */
    public function getProperties(object $object): iterable
    {
        $reflectionClass = $this->getReflectionClass($object);

        while ($reflectionClass instanceof ReflectionClass) {
            foreach ($this->getClassProperties($reflectionClass) as $property) {
                yield $property;
            }

            // We should check properties of parent class
            $reflectionClass = $reflectionClass->getParentClass();
        }
    }

    private function getReflectionClass(object $object): ReflectionClass
    {
        return new ReflectionClass($object);
    }

    /**
     * @return ReflectionProperty[]
     */
    private function getClassProperties(ReflectionClass $reflectionClass): array
    {
        return $reflectionClass->getProperties();
    }
}
