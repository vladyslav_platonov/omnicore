<?php

declare(strict_types=1);

namespace app\models;

use yii\base\DynamicModel;

/**
 * Base model to validate some specific object properties.
 */
class ObjectModel extends DynamicModel
{
    /**
     * @var object Object to be validated
     */
    private object $dto;

    public function __construct(object $dto, $config = [])
    {
        parent::__construct($config);

        $this->dto = $dto;
    }

    public function __get($name)
    {
        if (isset($this->dto->{$name})) {
            return $this->dto->{$name};
        }

        $getter = 'get' . ucfirst($name);

        if (method_exists($this->dto, $getter)) {
            return $this->dto->$getter();
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (isset($this->dto->{$name})) {
            $this->dto->{$name} = $value;
        }

        $setter = 'set' . ucfirst($name);

        if (method_exists($this->dto, $setter)) {
            $this->dto->$setter($value);
        }

        parent::__set($name, $value);
    }

    public function __isset($name)
    {
        return $this->hasAttribute($name) || parent::__isset($name);
    }

    public function canGetProperty($name, $checkVars = true, $checkBehaviors = true): bool
    {
        return parent::canGetProperty($name, $checkVars, $checkBehaviors)
            || $this->hasAttribute($name);
    }

    public function canSetProperty($name, $checkVars = true, $checkBehaviors = true): bool
    {
        return parent::canSetProperty($name, $checkVars, $checkBehaviors)
            || $this->hasAttribute($name);
    }

    public function hasAttribute($name): bool
    {
        $getter = 'get' . ucfirst($name);

        return isset($this->dto->{$name}) || method_exists($this->dto, $getter);
    }
}
