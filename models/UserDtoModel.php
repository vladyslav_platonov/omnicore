<?php

declare(strict_types=1);

namespace app\models;

use udokmeci\yii2PhoneValidator\PhoneValidator;

/**
 * Model to validate `UserDto` class instance.
 */
final class UserDtoModel extends ObjectModel
{
    public function rules(): array
    {
        return [
            [['id', 'firstName', 'lastName', 'phoneNumber', 'emailAddress'], 'required'],
            [['emailAddress'], 'email'],
            [['firstName', 'lastName'], 'string'],
            ['phoneNumber', PhoneValidator::class, 'country' => 'UA'],
        ];
    }
}
