<?php

declare(strict_types=1);

namespace app\services\user;

use app\lib\exceptions\ValidationFailedException;
use app\models\UserDtoModel;
use app\services\user\factories\UserDtoFactoryInterface;

/**
 * Current class creates `UserDto` class instance with `UserDtoFactoryInterface` class,
 *  then it validates DTO and returns errors if validation process failed.
 */
final class UserDtoProcessor implements UserDtoProcessorInterface
{
    private UserDtoFactoryInterface $userDtoFactory;

    public function __construct(UserDtoFactoryInterface $userDtoFactory)
    {
        $this->userDtoFactory = $userDtoFactory;
    }

    /**
     * @param array<string, mixed> $data Raw data (key => value)
     * @throws ValidationFailedException
     */
    public function execute(array $data): void
    {
        $dto = $this->userDtoFactory->create( $data);

        $model = new UserDtoModel($dto);

        if (false === $model->validate()) {
            throw new ValidationFailedException($model->getErrors());
        }
    }
}
