<?php

declare(strict_types=1);

namespace app\services\user;

/**
 * Contract to create and validate `UserDto` class instance.
 */
interface UserDtoProcessorInterface
{
    /**
     * @param array<string, mixed> $data Raw data (key => value)
     */
    public function execute(array $data): void;
}
