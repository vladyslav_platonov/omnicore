<?php

declare(strict_types=1);

namespace app\services\user\factories;

use app\lib\mapping\DataMapperInterface;
use UserDto;

/**
 * Helps to create `UserDto` class instance from raw data. Uses data mapper to set raw data into DTO class instance.
 */
final class UserDtoFactory implements UserDtoFactoryInterface
{
    private DataMapperInterface $dataMapper;

    public function __construct(DataMapperInterface $dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): UserDto
    {
        $dto = new UserDto();

        $this->dataMapper->map($dto, $data);

        return $dto;
    }
}
