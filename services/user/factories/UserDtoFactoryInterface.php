<?php

declare(strict_types=1);

namespace app\services\user\factories;

use UserDto;

/**
 * Helps to create `UserDto` class instance from raw data.
 */
interface UserDtoFactoryInterface
{
    /**
     * @param array<string, mixed> $data
     * @return UserDto
     */
    public function create(array $data): UserDto;
}
