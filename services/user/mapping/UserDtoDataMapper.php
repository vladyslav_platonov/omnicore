<?php

declare(strict_types=1);

namespace app\services\user\mapping;

use app\lib\mapping\DataMapperInterface;
use Yii;

/**
 * Helps to set raw data to `UserDto` class instance.
 */
final class UserDtoDataMapper implements DataMapperInterface
{
    private DataMapperInterface $mapper;

    public function __construct(DataMapperInterface $dataMapper)
    {
        $this->mapper = $dataMapper;
    }

    /**
     * Maps values from raw data to some specific object.
     *
     * @param object $object Object to set data
     * @param array<string, string> $data Raw data
     */
    public function map(object $object, array $data): void
    {
        $this->mapper->map($object, $this->prepareData($data));
    }

    /**
     * Current method maps data keys from input to dto properties.
     *
     * @param array $data Raw data
     * @return array Prepared data with good keys and default values
     */
    private function prepareData(array $data): array
    {
        $fieldMapping = Yii::$app->params['external']['dto']['mapping'];

        $preparedData = [];

        foreach ($fieldMapping as $property => $mappingInfo) {
            $preparedData[$property] = $data[$mappingInfo['key']] ?? $mappingInfo['default'];
        }

        return $preparedData;
    }
}
