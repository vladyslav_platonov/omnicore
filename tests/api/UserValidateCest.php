<?php

use Codeception\Util\HttpCode;

class UserValidateCest
{
    public function submitEmptyData(ApiTester $I): void
    {
        $I->sendPost('user/validate', []);
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseIsJson();
        $I->canSeeResponseContainsJson([
            'firstName' => ['First Name cannot be blank.'],
            'lastName' => ['Last Name cannot be blank.'],
            'phoneNumber' => ['Phone Number cannot be blank.'],
            'emailAddress' => ['Email Address cannot be blank.'],
        ]);
    }

    public function submitFirstInvalidData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/validate', [
                'id' => 1,
                'firstName' => 'Foo',
                'lastName' => 'Bar',
                'email' => 'valid@email.com',
                'phoneNumber' => '+308501111111',
        ]);
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseIsJson();
        $I->canSeeResponseContainsJson([
            'phoneNumber' => ['Phone number does not seem to be a valid phone number'],
        ]);
    }

    public function submitSecondInvalidData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/validate', [
                'id' => 1,
                'firstName' => 'Foo',
                'lastName' => 'Bar',
                'email' => 'notvalidemail@.com',
                'phoneNumber' => '+308501111111',
        ]);
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseIsJson();
        $I->canSeeResponseContainsJson([
            'emailAddress' => ['Email Address is not a valid email address.'],
            'phoneNumber' => ['Phone number does not seem to be a valid phone number'],
        ]);
    }

    public function submitValidData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/validate', [
                'id' => 1,
                'firstName' => 'Foo',
                'lastName' => 'Bar',
                'email' => 'valid@test.com',
                'phoneNumber' => '+380501111111',
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->canSeeResponseContainsJson([]);
    }
}
