<?php

declare(strict_types=1);

namespace tests\unit\services\user\mapping;

use app\lib\mapping\DataMapperInterface;
use app\services\user\factories\UserDtoFactory;
use Codeception\Test\Unit;
use UserDto;

class UserDtoFactoryTest extends Unit
{
    private DataMapperInterface $nextMock;
    private UserDtoFactory $factory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->nextMock = $this->createMock(DataMapperInterface::class);
        $this->factory = new UserDtoFactory($this->nextMock);
    }

    public function testDefault(): void
    {
        $requestData = ['test' => 'test'];

        $this->nextMock->expects(self::once())
            ->method('map')
            ->with(self::isInstanceOf(UserDto::class), $requestData);

        $this->factory->create($requestData);
    }
}
