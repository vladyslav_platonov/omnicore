<?php

declare(strict_types=1);

namespace tests\unit\services\user\mapping;

use app\lib\mapping\DataMapperInterface;
use app\services\user\mapping\UserDtoDataMapper;
use Codeception\Test\Unit;
use UserDto;

class UserDtoDataMapperTest extends Unit
{
    private DataMapperInterface $nextMock;
    private UserDtoDataMapper $mapper;

    protected function setUp(): void
    {
        parent::setUp();

        $this->nextMock = $this->createMock(DataMapperInterface::class);
        $this->mapper = new UserDtoDataMapper($this->nextMock);
    }

    public function testEmptyData(): void
    {
        $expectedData = ['id' => 0, 'firstName' => '', 'lastName' => '', 'emailAddress' => '', 'phoneNumber' => ''];
        $requestData = [];

        $dto = new UserDto();

        $this->nextMock->expects(self::once())
            ->method('map')
            ->with($dto, $expectedData);

        $this->mapper->map($dto, $requestData);
    }

    public function testGoodData(): void
    {
        $requestData = [
            'id' => 111,
            'firstName' => 'first',
            'lastName' => 'last',
            'email' => 'some@some.test',
            'phoneNumber' => 'phone'
        ];
        $expectedData = $requestData;
        $expectedData['emailAddress'] = $requestData['email'];
        unset($expectedData['email']);

        $dto = new UserDto();

        $this->nextMock->expects(self::once())
            ->method('map')
            ->with($dto, $expectedData);

        $this->mapper->map($dto, $requestData);
    }
}
